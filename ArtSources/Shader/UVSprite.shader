
Shader "Costum/UVSprite" 
{
	Properties 
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color("Color",Color) = (1,1,1,1)
		_Intensity("Intensity",float) = 1.0
	}
	
	CGINCLUDE

		#include "UnityCG.cginc"

		sampler2D _MainTex;
		
		uniform half4 _MainTex_ST;
		
		uniform half4 _Color;
		uniform fixed _Intensity;
		
		struct v2f 
		{
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
		};

		v2f vert(appdata_full v)
		{
			v2f o;
			
			o.uv.xy = v.texcoord.xy;			
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			
			return o;
		}
		
		fixed4 frag( v2f i ) : COLOR
		{	
			fixed4 outColor = tex2D(_MainTex, i.uv) *  _Color * _Intensity;
			
			return outColor;
		}
	
	ENDCG
	
	SubShader 
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha 
		//Cull Off
	Pass 
	{
		CGPROGRAM
		
		#pragma vertex vert
		#pragma fragment frag
		#pragma fragmentoption ARB_precision_hint_fastest 
		
		ENDCG
		 
		}
				
	} 
	FallBack Off
}
