
Shader "Costum/Laser" 
{
	Properties 
	{
		_MainTex ("Base", 2D) = "white" {}
	}
	
	CGINCLUDE

		#include "UnityCG.cginc"

		sampler2D _MainTex;
		half4 _MainTex_ST;
						
		struct v2f 
		{
			half4 pos : SV_POSITION;
			half2 uv : TEXCOORD0;
			half2 uvOrigin ;
		};

		v2f vert(appdata_full v) 
		{
			v2f o;
			
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);	
			o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
			o.uvOrigin = v.texcoord;
			return o; 
		}
		
		fixed4 frag( v2f i ) : COLOR 
		{	
			float par = (1.0f - (i.uvOrigin.x - 0.8f) / 0.2f);
			fixed4 col = tex2D (_MainTex, i.uv.xy);
			col.a *= saturate(par);
			return  col;
		}
	
	ENDCG
	
	SubShader 
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha 
		
	Pass {
	
		CGPROGRAM
		
		#pragma vertex vert
		#pragma fragment frag
		
		ENDCG
		 
		}
				
	} 
	FallBack Off
}
